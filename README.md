To setup OpenGL:
1. `go get github.com/go-gl/v4.5-core/gl`
2. `go get github.com/go-gl/glfw/v3.2/glfw`

To build and run each sample code:
1. `go build`
2. `sample_name.exe`

Any feedback please mailto: michael.icwang@gmail.com