package shaderloader

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/go-gl/gl/v4.5-core/gl"
)

type Shader struct {
	Type   uint32 // e.g. GL_VERTEX_SHADER
	Path   string // File path to shader source
	Shader uint32
}

func Load(shaders []Shader) uint32 {
	program := gl.CreateProgram()

	for _, s := range shaders {

		buf, err := ioutil.ReadFile(s.Path)
		if err != nil {
			panic(err)
		}

		// OpenGL requires shader source terminates with zero.
		source := string(buf) + "\x00"

		shader, err := compileShader(source, s.Type)
		if err != nil {
			log.Println("Failed to compile shader source:\n%s", source)
			panic(err)
		}

		gl.AttachShader(program, shader)

		s.Shader = shader
	}

	gl.LinkProgram(program)

	return program
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v, log: %v", source, log)
	}

	return shader, nil
}
