package main

import (
	"runtime"

	"github.com/go-gl/gl/v4.5-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"

	"gitlab.com/michael.wang/opengl_guide_golang/src/lib"
)

const (
	Triangles = iota
	NumVAOs   = iota
)

const (
	ArrayBuffer = iota
	NumBuffers  = iota
)

const (
	vPosition = iota
)

const (
	NumVertices = 6
)

var (
	window  *glfw.Window
	VAOs    [NumVAOs]uint32
	Buffers [NumBuffers]uint32
)

func init() {
	// GLFW needs to be called from same thread it was initialized.
	runtime.LockOSThread()

	err := glfw.Init()
	if err != nil {
		panic(err)
	}

	window, err = glfw.CreateWindow(800, 600, "Triangles", nil, nil)
	if err != nil {
		panic(err)
	}

	// Binding window to current thread. (see LockOSThread above.)
	window.MakeContextCurrent()
}

func init() {
	err := gl.Init()
	if err != nil {
		panic(err)
	}

	gl.CreateVertexArrays(NumVAOs, &VAOs[Triangles])
	gl.BindVertexArray(VAOs[Triangles])

	vertices := []float32{
		-0.90, -0.90, 0.85, -0.90, -0.90, 0.85, // Triangle 1
		0.90, -0.85, 0.90, 0.90, -0.85, 0.90, // Triangle 2
	}

	gl.CreateBuffers(NumBuffers, &Buffers[ArrayBuffer])
	gl.BindBuffer(gl.ARRAY_BUFFER, Buffers[ArrayBuffer])
	// Sample code use 'glBufferStorage', but according to book content, we should
	// be using 'glNamedBufferStorage' here.
	// 2nd argument needs number of bytes, so times 4 because float32 has 4 bytes.
	gl.NamedBufferStorage(Buffers[ArrayBuffer], 4*len(vertices), gl.Ptr(vertices), 0)

	shaders := []shaderloader.Shader{
		{
			gl.VERTEX_SHADER,
			"../../bin/media/shaders/triangles/triangles.vert",
			0,
		},
		{
			gl.FRAGMENT_SHADER,
			"../../bin/media/shaders/triangles/triangles.frag",
			0,
		},
	}
	program := shaderloader.Load(shaders)
	gl.UseProgram(program)

	gl.VertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, nil)
	gl.EnableVertexAttribArray(vPosition)
}

func display() {
	black := []float32{0.0, 0.0, 0.0, 0.0}
	gl.ClearBufferfv(gl.COLOR, 0, &black[0])

	gl.BindVertexArray(VAOs[Triangles])
	gl.DrawArrays(gl.TRIANGLES, 0, NumVertices)
}

func main() {
	defer glfw.Terminate()

	for !window.ShouldClose() {

		display()

		window.SwapBuffers()

		glfw.PollEvents()
	}
}
